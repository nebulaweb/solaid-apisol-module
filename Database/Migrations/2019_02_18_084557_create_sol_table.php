<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSolTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sol', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_user');
            $table->foreign('id_user')
                ->references('id')
                ->on('users')
                ->onDelete('restrict')
                ->onUpdate('restrict');
            $table->float('lat')->nullable();
            $table->float('lon')->nullable();
            $table->integer('id_parcel');
            $table->foreign('id_parcel')
                ->references('id')
                ->on('parcel')
                ->onDelete('restrict')
                ->onUpdate('restrict');
            $table->integer('uts_id');
            $table->string('uts_def');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sol', function(Blueprint $table) {
            $table->dropForeign(['id_user']);
            $table->dropForeign(['id_parcel']);
        });
        Schema::dropIfExists('sol');
    }
}
