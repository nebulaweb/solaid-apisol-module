<?php

namespace Modules\APISol\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Modules\APISol\Entities\Sol;

class APISolController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function viewer(Request $request)
    {
        return view('apisol::viewer', [
            'path' => module_path('APISol')
        ]);
    }

    public function setUts(Request $request)
    {

        $sol = new Sol();
        if($request->query->has('uts_id')) {
            $sol->uts_id = $request->query->get('uts_id');
        }
        if($request->query->has('uts_def')) {
            $sol->uts_def = $request->query->get('uts_def');
        }
        if($request->query->has('id_parcel')) {
            $sol->id_parcel = $request->query->get('id_parcel');
        }
        if($request->query->has('id_user')) {
            $sol->id_user = $request->query->get('id_user');
        }
        if($request->query->has('lat')) {
            $sol->lat = $request->query->get('lat');
        }
        if($request->query->has('lon')) {
            $sol->lon = $request->query->get('lon');
        }

        $sol->save();


        return response()->json([
            'id' => $sol->id,
            'uts_id' => $sol->uts_id,
            'uts_def' => $sol->uts_def,
            'id_parcel' => $sol->id_parcel
        ]);
    }
}
