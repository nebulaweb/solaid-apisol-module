@extends('apisol::layouts.layout')

@section('content')

  <div class="container">
    <h3>
        This view is loaded from module: {!! config('apisol.name') !!} located in {{ $path }}
    </h3>

    <form action="" id="apisol-form">
      <input type="text" id="apisol-lat" name="apisol-lat" value="48.352598707539315" placeholder="lat">
      <input type="text" id="apisol-lon" name="apisol-lon" value="-3.4689331054687504" placeholder="lon">
      <button type="button" id="apisol-open" class="btn btn-primary">Tester la fenêtre APISol</button>
    </form>
    <div id="modal-apisol"></div>
  </div>

@endsection

@section('javascript')
  <script type="text/javascript">

    var apisol = $('#modal-apisol').APISol();

    $('#apisol-open').on('click', function(e) {
      var lat = $('#apisol-lat').val();
      var lon = $('#apisol-lon').val();
      apisol.load(lat, lon, 4326);
      e.preventDefault();
    })

    apisol.on('uts:confirm', function(e, uts) {

      var url = '{{ route("apisol_setuts") }}';
      $.get({
        url: url,
        data : {uts_id: uts.no_us, uts_def: uts.nom_us, lat: uts.y, lon: uts.x, id_user: 1, id_parcel:1 },
        dataType: 'json',
        success: function(json) {
          console.log(json);
        },
        error: function(err) {
          console.log(err);
        }
      });

    })
    ;

  </script>
@endsection