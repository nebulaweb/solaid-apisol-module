<?php

namespace Modules\APISol\Entities;

use Illuminate\Database\Eloquent\Model;

class Sol extends Model
{
    protected $fillable = [];

    protected $table = 'sol';
}
