<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'apisol',  'middleware' => ['jwt.login','auth.required']], function() {

  Route::get('/', 'APISolController@viewer');
  Route::get('/viewer', 'APISolController@viewer')->name('apisol_viewer');
  Route::get('/setuts', 'APISolController@setUts')->name('apisol_setuts');

});
